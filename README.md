# tiendas
App Django para probar permisos personalizados.

# Ejecutar servidor
`poetry run python manage.py runserver`

# Ejecutar pruebas
`poetry run pytest`

# Endpoints
* `/tiendas/` Operación de listado (GET) e inserción (PUT) de tiendas.
* `/tiendas/<id>/` Operaciones de consulta (GET), edición total (PUT) o parcial (PATCH) o eliminación (DELETE) de una tienda en concreto.
