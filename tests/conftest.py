import pytest
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from tiendas.api.models import Tienda


@pytest.fixture
def client():
    # pytest-django fixture uses default Django APIClient, not rest_framework's
    return APIClient()


@pytest.fixture
def user_normal(db):
    return User.objects.create_user("User1")


@pytest.fixture
def user_manager(db):
    return User.objects.create_user("Manager1")


@pytest.fixture
def user_manager2(db):
    return User.objects.create_user("Manager2")


@pytest.fixture
def user_superuser(db):
    return User.objects.create_superuser("Superuser1")


@pytest.fixture
def tienda_user(db, user_manager):
    my_tienda = Tienda.objects.create(name="Tienda1")
    my_tienda.managers.add(user_manager)
    return my_tienda


@pytest.fixture
def tienda_user2(db, user_manager2):
    my_tienda = Tienda.objects.create(name="Tienda2")
    my_tienda.managers.add(user_manager2)
    return my_tienda


@pytest.fixture
def tienda_superuser(db, user_superuser):
    my_tienda = Tienda.objects.create(name="TiendaSuperUser")
    my_tienda.managers.add(user_superuser)
    return my_tienda
