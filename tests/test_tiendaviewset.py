from django.urls import reverse


class TestTiendaViewSet:
    def test_unauth_cant_retrieve(self, client, tienda_user):
        response = client.get(reverse("tienda-detail", args=[tienda_user.id]))
        assert response.status_code == 403

    def test_user_can_retrieve(self, client, tienda_user, user_normal):
        client.force_login(user_normal)
        response = client.get(reverse("tienda-detail", args=[tienda_user.id]))
        assert response.status_code == 200
        assert response.data['name'] == tienda_user.name

    def test_user_cant_edit(self, client, tienda_user, user_normal):
        client.force_login(user_normal)
        response = client.patch(
            reverse("tienda-detail", args=[tienda_user.id]), {"name": "newname"}
        )
        assert response.status_code == 403

    def test_manager_can_edit(self, client, tienda_user, user_manager):
        client.force_login(user_manager)
        response = client.patch(
            reverse("tienda-detail", args=[tienda_user.id]), {"name": "newname"}
        )
        assert response.status_code == 200
        assert response.data["name"] == "newname"

    def test_manager_cant_edit_another_non_managed(
        self, client, tienda_user2, user_manager
    ):
        client.force_login(user_manager)
        response = client.patch(
            reverse("tienda-detail", args=[tienda_user2.id]), {"name": "newname"}
        )
        assert response.status_code == 403

    def test_superuser_can_edit_any_tienda(
        self, client, user_superuser, tienda_user, tienda_superuser
    ):
        client.force_login(user_superuser)
        response = client.patch(
            reverse("tienda-detail", args=[tienda_user.id]), {"name": "superwashere1"}
        )
        assert response.status_code == 200
        assert response.data["name"] == "superwashere1"
        response = client.patch(
            reverse("tienda-detail", args=[tienda_user.id]), {"name": "superwashere2"}
        )
        assert response.status_code == 200
        assert response.data["name"] == "superwashere2"
