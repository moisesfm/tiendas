from django.apps import AppConfig


class TiendasApiConfig(AppConfig):
    name = 'tiendas_api'
