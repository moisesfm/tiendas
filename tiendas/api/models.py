from django.db import models
from django.contrib.auth.models import User


class Tienda(models.Model):
    name = models.CharField(max_length=50)
    managers = models.ManyToManyField(User)
