from rest_framework import status, viewsets
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from .models import Tienda
from .serializers import TiendaSerializer


class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


class IsManager(BasePermission):
    message = 'Only Manager can edit Tiendas.'

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser or request.method in SAFE_METHODS:
            return True
        return request.user in obj.managers.all()


class TiendaViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated & IsManager]
    queryset = Tienda.objects.all()
    serializer_class = TiendaSerializer
