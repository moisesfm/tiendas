"""tiendas URL Configuration

"""
from django.contrib import admin
from django.urls import path
from tiendas.api.urls import router

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('api/', include('tiendas.api.urls'))
]

urlpatterns += router.urls
